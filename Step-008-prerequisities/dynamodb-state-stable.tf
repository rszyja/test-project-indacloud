resource "aws_dynamodb_table" "dynamodb_terraform_lock" {
  hash_key = "LockID"
  name = "indacloud_lock_table"
  read_capacity = 5
  write_capacity = 5


  attribute {
    name = "LockID"
    type = "S"
  }
}