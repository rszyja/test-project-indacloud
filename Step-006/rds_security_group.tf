locals {
  postgres_security_group_name = "postgres-inbound-traffic"
}

resource "aws_security_group" "rds_access" {
  name        = local.postgres_security_group_name
  description = "Allow postgresinbound traffic"
  vpc_id      = aws_vpc.indacloud-stepbystep.id

  ingress {
    description = "postgres inbound rule"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [
      aws_vpc.indacloud-stepbystep.cidr_block
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = local.postgres_security_group_name
    Terraform = "true"
  }
}