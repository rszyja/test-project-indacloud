resource "aws_rds_cluster_instance" "rds_write_node" {
  cluster_identifier = aws_rds_cluster.rds_cluster.id
  instance_class = "db.t3.medium"

  engine = aws_rds_cluster.rds_cluster.engine
  engine_version = aws_rds_cluster.rds_cluster.engine_version

  db_subnet_group_name = aws_db_subnet_group.rds_subnet_group.name
  apply_immediately = true


  availability_zone = "eu-central-1a"
}


resource "aws_rds_cluster_instance" "rds_read_node" {
  cluster_identifier = aws_rds_cluster.rds_cluster.id
  instance_class = "db.t3.medium"

  engine = aws_rds_cluster.rds_cluster.engine
  engine_version = aws_rds_cluster.rds_cluster.engine_version

  db_subnet_group_name = aws_db_subnet_group.rds_subnet_group.name
  apply_immediately = true


  availability_zone = "eu-central-1b"
}