output "vpc_id" {
  value = aws_vpc.indacloud-stepbystep.id
}

output "vpc_arn" {
  value = aws_vpc.indacloud-stepbystep.arn
}

output "bastion_ip" {
  value = aws_eip.bastion_eip.public_ip
}

output "rds_write_endpoit" {
  value = aws_rds_cluster_instance.rds_write_node.endpoint
}

output "rds_read_endpoint" {
  value = aws_rds_cluster_instance.rds_read_node.endpoint
}
