terraform {
  backend "s3" {
    bucket = "state-management-indacloud-run3"
    key    = "test-stack/state/terraform.tfstate"
    region = "eu-central-1"
    profile = "default"
    dynamodb_table = "indacloud_lock_table"
  }
}

data "terraform_remote_state" "my_state" {
  backend = "s3"
  config = {
    profile = "default"
    bucket = "state-management-indacloud-run3"
    key    = "test-stack/state/terraform.tfstate"
    region = "eu-central-1"
  }
}
