provider "aws" {
  profile    = "default"
  region     = var.aws_default_region
}
